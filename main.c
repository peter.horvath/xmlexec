#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>
#include <gmodule.h>

#ifdef DEBUG
#define debug(...) printf(__VA_ARGS__)
#else
#define debug(...)
#endif

#define OMIT_OPENING_TAG 1

typedef enum {
  FD_MODE_NONE = 1,
  FD_MODE_XML = 2,
  FD_MODE_CREATE = 3,
  FD_MODE_APPEND = 4
} fd_mode;

typedef enum {
  RESULT_OK = 1,
  RESULT_OK_END = 2,
  RESULT_ERROR = 3
} node_process_result;

void die(const char* type, const char* msg) {
  fprintf (stderr, "%s: %s\n", type, msg);
  exit (1);
}

gchar** g_slist_to_array(GSList *slist) {
  int len = g_slist_length(slist);
  gchar** rval = (char**)g_malloc(sizeof(char*)*(len+1));
  int p=0;
  while (slist) {
    rval[p] = slist->data;
    slist = slist->next;
    p++;
  }
  rval[len]=NULL;
  return rval;
}

void xml_next_element(xmlTextReaderPtr reader) {
  while (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    int ret = xmlTextReaderRead(reader);
    if (ret < 0) {
      die ("xml parse error", "");
    }
  }
}

static gchar* input = NULL;
static GOptionEntry entries[] = {
  { "input", 'i', 0, G_OPTION_ARG_STRING, &input, "Xml file dump, use \"-\" for stdin, default stdin", "input" },
  { NULL }
};

int multiMode = 0;

gint gStrCompareFunc(gconstpointer a, gconstpointer b) {
  return g_strcmp0(a, b);
};

void processArgs(int argc, char** argv) {
  GError* error = NULL;
  GOptionContext* context;
  context = g_option_context_new("xargs clone using xml input");
  g_option_context_add_main_entries(context, entries, NULL);
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    die ("invalid options", error->message);
  }
}

struct task {
  gchar* path;
  gchar* id;
  GPtrArray* args;
  GTree* env;
  int stdin_mode;
  gchar* stdin_path;
  gchar* stdin_content;
  int stdout_mode;
  gchar* stdout_path;
  gchar* stdout_content;
  int stderr_mode;
  gchar* stderr_path;
  gchar* stderr_content;
  pid_t pid;
  int exit_code;
};

struct task* read_task(xmlTextReaderPtr reader, int flags) {
  /*
  struct task* task = g_new(struct task, 1);
  if (!(flags|OMIT_OPENING_TAG)) {
    xml_next_element(reader);
  }
  task->path = NULL;
  task->id = NULL;
  task->args = g_ptr_array_new();
  task->env = g_tree_new(gStrCompareFunc);
  task->stdin_mode = FD_MODE_NONE;
  task->stdin_path = NULL;
  task->stdin_content = NULL;
  task->stdout_mode = FD_MODE_NONE;
  task->stdout_path = NULL;
  task->stdout_content = NULL;
  task->stderr_mode = FD_MODE_NONE;
  task->stderr_path = NULL;
  task->stderr_content = NULL;
  task->pid = -1;
  task->exit_code = 0;

  int ret = xmlTextReaderMoveToFirstAttribute(reader);
  if (ret != 1)
    die ("xml parse error", ret ? "error" : "no attributes");

  for (;;) {
    xmlChar* name = xmlTextReaderLocalName(reader);
    xmlChar* value = xmlTextReaderReadAttributeValue(reader);
  }

  int attr_no = xmlTextReaderAttributeCount(reader);
  for (int i=0; i < attr_no; i++) {
    xmlChar* name = xmlTextReaderLocalName(reader);
    xmlChar* value = 
  }*/
  return NULL;
};

void execute_task(struct task* task) {
};

void free_task(struct task* tas) {
};

int processNode(xmlTextReaderPtr reader, void (*handler)(xmlTextReaderPtr reader, int type, int depth, xmlChar* name)) {
  int ret = xmlTextReaderRead(reader);
  if (ret == 0)
    return 0;

  if (ret == -1)
    die ("xml read error", "");

  int type = xmlTextReaderNodeType(reader);
  int depth = xmlTextReaderDepth(reader);
  xmlChar* name = xmlTextReaderName(reader);
  debug("node, element type: %i, depth: %i, name: %s\n", type, depth, name);
  if (type != XML_READER_TYPE_ELEMENT && type != XML_READER_TYPE_END_ELEMENT && type != XML_READER_TYPE_TEXT && type != XML_READER_TYPE_CDATA)
    return 0;

  if (type == XML_READER_TYPE_ELEMENT || type == XML_READER_TYPE_END_ELEMENT)
    name = xmlStrdup(name);
  else
    name = NULL;
  handler(reader, type, depth, name);
  if (name)
    xmlFree(name);

  int endDepth = xmlTextReaderDepth(reader);
  if (endDepth != depth)
    die("depth change on node processing, baad", "");

  return 0;
};

int processChilds(xmlTextReaderPtr reader, void (*handler)(xmlTextReaderPtr reader, int type, int depth, xmlChar* name)) {
  int ret;

  int childNodeHandler(xmlTextReaderPtr reader, int childType, int childDepth, xmlChar* childName) {
    if (childType == XML_READER_TYPE_END_ELEMENT) {
      debug("childNodeHandler found /%s, depth: %i", childName, childDepth);
      if (childDepth != depth)
        die ("xml parse error", "invalid end tag depth");
      if (g_strcmp0(childName, name))
        die ("xml parse error", "invalid end tag name");
      return RESULT_OK_END;
    }
    ret = handler(reader, childType, childDepth, childName);
    return ret;
  };
  for (;;) {
    ret = processNode(reader, childNodeHandler);
    debug ("processChilds -> childNodeHandler returns %i\n", ret);
    if (ret == RESULT_OK_END)
      return RESULT_OK;
    else if (ret == RESULT_OK)
      continue;
    else
      die ("xml parse error", "processChilds -> processNode bad result");
  }
};

int dumpNodeHandler(xmlTextReaderPtr reader, int type, int depth, xmlChar* name) {
  debug ("dumpNodeHandler, type: %i, depth: %i, name: %s", type, depth, name);
  return RESULT_OK;
};

int taskNodeHandler(xmlTextReaderPtr reader, int type, int depth, xmlChar* name) {
  int ret = processChilds(reader, dumpNodeHandler, type, depth, name);
  return ret;
};

int topNodeHandler(xmlTextReaderPtr reader, int type, int depth, xmlChar* name) {
  int ret;
  if (depth != 0)
    die ("xml error", "topNodeHandler with non-zero depth");
  if (type != XML_READER_TYPE_ELEMENT)
    die ("xml error", "element expected");
  if (!g_strcmp0(name, "tasks")) {
    multiMode = 1;
    ret = processChilds(reader, taskNodeHandler, type, depth, name);
    debug ("topNodeHandler processChilds returns %i\n", ret);
    return ret;
  } else if (!g_strcmp0(name, "task")) {
    multiMode = 0;
    ret = taskNodeHandler(reader, type, depth, name);
    debug ("topNodeHandler -> taskNodeHandler returns %i\n", ret);
    return ret;
  } else {
    die ("xml error", "task | tasks root node expected");
  }
};

int main(int argc, char** argv) {
  int inputFd;
  int ret;

  processArgs(argc, argv);

  if (!input || !input[0] || !strcmp(input,"-")) {
    inputFd = STDIN_FILENO;
  } else {
    inputFd = open(input, O_RDONLY);
    if (inputFd == -1) {
      die ("can not open input file", strerror(errno));
    }
  }

  debug ("inputFd: %i\n", inputFd);

  xmlTextReaderPtr reader = xmlReaderForFd(inputFd, NULL, "UTF-8", XML_PARSE_PEDANTIC);
  if (!reader) {
    die ("can not initialize xmlReaderForFd on input", strerror(errno));
  }

  ret = processNode(reader, topNodeHandler);
  debug("top processNode returns %i\n", ret);
  if (ret != RESULT_OK_END) {
    die("xml parse error", "topNodeHandler is not OK_END");
  }

  /*
  xml_next_element(reader);

  debug ("firstNode\n");
  const gchar* name = (const gchar*)xmlTextReaderConstName(reader);
  debug ("first node name is: %s\n", name);

  struct task *task = NULL;

  if (!g_strcmp0(name, "tasks")) {
    while ((task = read_task(reader, 0))) {
      execute_task(task);
      free_task(task);
    }
  } else if (!g_strcmp0(name, "task")) {
    task = read_task(reader, OMIT_OPENING_TAG);
    execute_task(task);
    free_task(task);
  } else {
    die ("xml parse error", "root node must be task or tasks");
  }
  */

  xmlFreeTextReader(reader);

  if (inputFd != STDIN_FILENO) {
    close(inputFd);
  }

  return 0;
}
